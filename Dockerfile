FROM ubuntu:18.04
RUN apt-get update \
  && apt-get install -y curl git vim build-essential \
    zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev \
    libreadline-dev libffi-dev wget \
  && curl https://pyenv.run | bash \
  && /root/.pyenv/bin/pyenv install 3.6.8 \
  && /root/.pyenv/bin/pyenv local 3.6.8 \
  && /root/.pyenv/shims/python3 -m pip install --user --upgrade pip \
  && /root/.pyenv/shims/pip3 install --user virtualenv \
  && /root/.pyenv/shims/python3 -m pip --version \
  && /root/.pyenv/shims/python3 -m pip install --user tensorflowjs \
  && echo 'export PATH="/root/.local/bin:$PATH"' >> ~/.bashrc \
  && echo 'export PATH="/root/.pyenv/bin:$PATH"' >> ~/.bashrc \
  && echo 'eval "$(/root/.pyenv/bin/pyenv init -)"' >> ~/.bashrc \
  && echo 'eval "$(/root/.pyenv/bin/pyenv virtualenv-init -)"' >> ~/.bashrc \
  && curl -sL https://deb.nodesource.com/setup_12.x | bash -  \
  && apt-get install -y nodejs \
  && node --version && \
  && npm install -g jest prettier
