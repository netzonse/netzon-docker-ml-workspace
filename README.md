# netzon-docker-ml-workspace
Docker workspace for ML tasks.

```bash
sudo docker tag netzon-docker-ml-workspace:latest netzon/netzon-docker-ml-workspace:latest
sudo docker push netzon/netzon-docker-ml-workspace:0.0.1
sudo docker login
```